# Spring - Microservice Messagerie

Dans de la cadre de l'intégration de Comiti pour un stage en entreprise de 2 mois, j'ai réçu un test a effectuer.

# Création d'un microservice

J'ai choisi la fonctionnalité 'Messagerie', implémenté avec en Java avec le Framwork Spring.
Ce microservice utilise une base de donnée Mysql 5.7.

# Mise en place de l'environnement

Grâce à la documentation de Spring, j'ai mis en place le dépendences nécessaires au bon fonctionnement de mon projet.
- Spring web pour la création du service REST
- Lombock pour les annotations getters & setters
- JPA  pour la manipulation des tables
- HSQL pour la liason avec la base de donnée

# Développement
Développement de l'organisation du code pour l'implémentation du microservice :
- Model -> un 'Message' contient à un 'expéditeur' , un 'destinataire', un 'contenu' et la possibilité d'être lu ou non lu.
- Controller -> Récupération, Creation, Suppression, Modification suivant l'url donnée
- Repository -> Méthodes CRUD pour les informations en base de donnée.
- Service -> Autres traitements et vérifications
- Exception -> Lever d'exceptions
