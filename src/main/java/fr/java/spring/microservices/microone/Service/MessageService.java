package fr.java.spring.microservices.microone.Service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.java.spring.microservices.microone.Exception.EmptyException;
import fr.java.spring.microservices.microone.Exception.NotFoundException;
import fr.java.spring.microservices.microone.Exception.NotModifiedException;
import fr.java.spring.microservices.microone.Model.Message;
import fr.java.spring.microservices.microone.Repository.MessageRepository;

@Service
public class MessageService {

    @Autowired MessageRepository messageRepository; //injection dépendence

    /**
     * Récupère en bdd les messages
     */
    public Iterable<Message> findAll() throws EmptyException {

        Iterable<Message> messages = this.messageRepository.findAll();
        if(!messages.iterator().hasNext()) throw new EmptyException();
        return this.messageRepository.findAll();
    }

    /**
     * Post en bdd un message
     * @param message
     * @return
     */
    public Message postOne(Message message) throws NotFoundException {
        if(message == null) throw new NotFoundException();
        return this.messageRepository.save(message);
    }

    /**
     * Edit un message et l'update en bdd
     * @param message
     * @return
     */
    public Message editOne(Message message) throws NotModifiedException {
        if(message == null) throw new NotModifiedException();
        return this.messageRepository.save(message);
    }

    /**
     * Supprime un message en bdd
     * @param id
     */
    public void deleteOne(Long id) throws NotFoundException {
        if(id == null) throw new NotFoundException();
        this.messageRepository.deleteById(id);
    }

}
