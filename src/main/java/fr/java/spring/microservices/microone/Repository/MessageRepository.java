package fr.java.spring.microservices.microone.Repository;

import org.springframework.data.repository.CrudRepository;

import fr.java.spring.microservices.microone.Model.Message;

/**
 * Repository qui permet l'utilisation du crud pour l'accès au données
 */
public interface MessageRepository extends CrudRepository<Message, Long> {
}
