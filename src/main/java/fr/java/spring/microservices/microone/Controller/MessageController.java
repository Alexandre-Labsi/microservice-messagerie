package fr.java.spring.microservices.microone.Controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import fr.java.spring.microservices.microone.Exception.EmptyException;
import fr.java.spring.microservices.microone.Exception.NotFoundException;
import fr.java.spring.microservices.microone.Exception.NotModifiedException;
import fr.java.spring.microservices.microone.Model.Message;
import fr.java.spring.microservices.microone.Service.MessageService;


@RestController
public class MessageController {

    @Autowired MessageService messageService;

    /**
     * Récupère une liste de Message
     * @return la liste des messages
     */
    @GetMapping(path = "/messages")
    private Iterable<Message> getMessages() throws EmptyException {

        return this.messageService.findAll();
    }
    
    /**
     * Post un message
     * @param message
     * @return message
     */
    @PostMapping(path = "/create-message")
    private Message postMessage(@RequestBody Message message) throws NotFoundException {

        return this.messageService.postOne(message);
    }

    /**
     * Edit le message
     * @return message edited
     */
    @PutMapping(path = "/see")
    private Message editMessage(@RequestBody Message message) throws NotModifiedException {

        return this.messageService.editOne(message);
    }

    /**
     * Supprime un message par son ID
     * @param id
     * @return void
     */
    @DeleteMapping(path = "/delete-message/{id}")
    private void deleteMessage(@PathVariable Long id) throws NotFoundException {
    
        this.messageService.deleteOne(id);
    }




}
