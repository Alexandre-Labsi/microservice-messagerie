package fr.java.spring.microservices.microone.Model;

import javax.persistence.Entity;
import javax.persistence.Id;

import lombok.Data;

@Entity //hibernate orm
@Data //auto-getter&setter => lombock
public class Message {

    @Id
    private Long id;
    private String from;
    private String to;
    private String content;
    private Boolean see;
}
